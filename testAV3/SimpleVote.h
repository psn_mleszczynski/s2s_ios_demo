//
//  SimpleRate.h
//  SP2app
//
//  Created by Cedric Hamelin on 29/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Filters.h"

#define runDelegate(clss,sel,input,output) {\
if (!clss) {\
NSLog(@"run > ERROR, %@ class not found!",[[clss class] description]);\
}\
else if ([clss respondsToSelector:sel]) {\
NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:\
[[clss class] instanceMethodSignatureForSelector:sel]];\
[invocation setSelector:sel];\
[invocation setTarget:clss];\
if (input!=nil) {\
[invocation setArgument:input atIndex:2];\
}\
[invocation invoke];\
if (output!=nil) {\
[invocation getReturnValue:output];\
}\
}\
}\



@interface SimpleVote: UIView {
    int _w,_h,_x,_y;
    id _delegate;
    
    UIImage *imON1;
    UIImage *imON2;
    UIImage *imOFF1;
    UIImage *imOFF2;
    
    UILabel *title;
    UIButton *blockView1;
    UIButton *blockView2;
    
    CGRect _frameRight;
    CGRect _frameLeft;
    CGRect _frameCenter;
    
    UIImageView *puff;

}

-(id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate andWithTitle:(NSString *)title andWithImage1:(UIImage *)image1 andWithImage2:(UIImage *)image2;

-(void)drawHorizontal;
-(void)drawVertical;


@end
