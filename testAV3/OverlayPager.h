//
//  OverlayPager.h
//  SP2app
//
//  Created by Cedric Hamelin on 29/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverlayPager : UIScrollView <UIScrollViewDelegate> {
    int _w,_h,_x,_y;

    NSInteger currentPage;

    
}
@property (assign) CGRect subViewRect;


-(void)drawHorizontal;
-(void)drawVertical;


@end
