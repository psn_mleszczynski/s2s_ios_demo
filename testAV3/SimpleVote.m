//
//  SimpleRate.m
//  SP2app
//
//  Created by Cedric Hamelin on 29/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//


// THIS IS ONLY EXAMPLE CODE
/* You may use this code as an example to get you inspired of how to display the payload for a VOTE widget */

#import "SimpleVote.h"

@implementation SimpleVote

- (id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate
                                  andWithTitle:(NSString *)titleText
                                  andWithImage1:(UIImage *)image1
                                  andWithImage2:(UIImage *)image2
    
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame=frame;
        //NSLog(@"rate > (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
        _w = frame.size.width;
        _h = frame.size.height;
        _x = frame.origin.x;
        _y = frame.origin.y;
        _delegate = delegate;
            
        // Initialization code
        int _imgh = _h-70;              //image heigth
        int _imgw = _w/2-20;          //_imgh*3/4;          //image width
        int _tw=_w-40;                  //title ctrl width

        // RATING TITLE
        title = [[UILabel alloc] initWithFrame:CGRectMake((_w -_tw)/2, 0, _tw, 42)];
        //[title setCenter:CGPointMake((_w + _imgw)/2-150,25)];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setText:titleText];
        [title setTextColor:[UIColor whiteColor]];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:32.0f]];
        //title.layer.borderColor = [UIColor redColor].CGColor;
        //title.layer.borderWidth = 2.0;
        
        
        //description.layer.borderColor = [UIColor redColor].CGColor;
        //description.layer.borderWidth = 2.0;
        

        imON1 = [image1 copy];
        imON2 = [image2 copy];
        imOFF1 = [[image1 copy] saturateImage:0.0 withContrast:1.0];
        imOFF2 = [[image2 copy] saturateImage:0.0 withContrast:1.0];

        _frameLeft = CGRectMake(20,50,_imgw,_imgh);
        _frameCenter = CGRectMake((_w -_imgw)/2,50,_imgw,_imgh);
        _frameRight = CGRectMake(_w-_imgw-20,50,_imgw,_imgh);

        // IMAGE LEFT
        blockView1 = [[UIButton alloc] initWithFrame:_frameLeft];
        blockView1.contentMode = UIViewContentModeScaleAspectFit;
        [blockView1 setImage:imOFF1 forState:UIControlStateNormal];
        [blockView1 setImage:imON1 forState:UIControlStateSelected];
        [blockView1 setImage:imON1 forState:UIControlStateHighlighted];
        [blockView1 setImage:imON1 forState:UIControlStateDisabled];
        [blockView1 setAdjustsImageWhenDisabled:NO];
        [blockView1 addTarget:self action:@selector(selectImage1) forControlEvents:UIControlEventTouchUpInside];
        [blockView1 setAlpha:1.0f];
        
        // IMAGE RIGHT
        blockView2 = [[UIButton alloc] initWithFrame:_frameRight];
        blockView2.contentMode = UIViewContentModeScaleAspectFit;
        [blockView2 setImage:imOFF2 forState:UIControlStateNormal];
        [blockView2 setImage:imON2 forState:UIControlStateSelected];
        [blockView2 setImage:imON2 forState:UIControlStateHighlighted];
        [blockView2 setImage:imON2 forState:UIControlStateDisabled];
        [blockView2 setAdjustsImageWhenDisabled:NO];
        [blockView2 addTarget:self action:@selector(selectImage2) forControlEvents:UIControlEventTouchUpInside];
        [blockView2 setAlpha:1.0f];
        [self addSubview:title];
        [self addSubview:blockView1];
        [self addSubview:blockView2];
        
        puff = [self explosion];
        //self.layer.borderColor = [UIColor yellowColor].CGColor;
        //self.layer.borderWidth = 2.0;

    }
    return self;
}




-(void)drawHorizontal {
        NSLog(@"vote > drawHorizontal");
        int _imgh = _h-70;              //image heigth
        int _imgw = _w/2-20;          //_imgh*3/4;          //image width
        int _tw=_w-40;                  //title ctrl width
    
        title.frame =CGRectMake((_w -_tw)/2, 0, _tw, 42);
        _frameLeft = CGRectMake(20,50,_imgw,_imgh);
        _frameCenter = CGRectMake((_w -_imgw)/2,50,_imgw,_imgh);
        _frameRight = CGRectMake(_w-_imgw-20,50,_imgw,_imgh);
    
        blockView1.frame = _frameLeft;
        blockView2.frame = _frameRight;
    
}

-(void)drawVertical {
        NSLog(@"vote > drawVertical");
        int _imgh = _h-70;              //image heigth
        int _imgw = _w/2-20;          //_imgh*3/4;          //image width
        int _tw=_h-20;                  //title ctrl width
    
        title.frame =CGRectMake((_h -_tw)/2, 0, _tw, 42);
        _frameLeft = CGRectMake(20,50,_imgw,_imgh);
        _frameCenter = CGRectMake((_h -_imgh)/2,50,_imgw,_imgh);
        _frameRight = CGRectMake(_h-_imgh-20,50,_imgw,_imgh);
    
        blockView1.frame = _frameLeft;
        blockView2.frame = _frameRight;

}




- (UIImageView *)explosion{
    UIImageView *_explosion = [[UIImageView alloc] initWithFrame:self.bounds];

    _explosion.animationImages =  @[[UIImage imageNamed:@"puff_01.png"],
                                    [UIImage imageNamed:@"puff_02.png"],
                                    [UIImage imageNamed:@"puff_03.png"],
                                    [UIImage imageNamed:@"puff_04.png"],
                                    [UIImage imageNamed:@"puff_05.png"]];

    _explosion.animationDuration = 0.5;
    _explosion.animationRepeatCount = 1;
    return _explosion;
}



-(void)selectImage1{
    NSLog(@"vote > select 1");
    
    // explode image2
    [puff setFrame:_frameRight];
    [self addSubview:puff];
    [puff startAnimating];
    
    [blockView1 setUserInteractionEnabled:NO];
    [blockView2 setUserInteractionEnabled:NO];
    [blockView1 setSelected:YES];
    [blockView2 setSelected:NO];
    

    
    [UIView animateWithDuration:0.5 delay:0.0 options:0 animations:^{
         // Animate the alpha value of your imageView from 1.0 to 0.0 here
         blockView2.alpha = 0.0f;
     } completion:^(BOOL finished) {
         // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
         blockView2.hidden = YES;
         [blockView2 removeFromSuperview];
         [UIView animateWithDuration:0.5 delay:0.0 options:0 animations:^{
            [blockView1 setFrame:_frameCenter];
          } completion:^(BOOL finished) {
          }];
     }];
    NSDictionary *data=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithUnsignedLong:(unsigned long)1],@"vote", nil];
    runDelegate(_delegate, @selector(processFeedBack:),&data,nil);
    
    
    
}


-(void)selectImage2{
    NSLog(@"vote > select 2");
    
    // explode image1
    [puff setFrame:_frameLeft];
    [self addSubview:puff];
    [puff startAnimating];
    [blockView1 setUserInteractionEnabled:NO];
    [blockView2 setUserInteractionEnabled:NO];
    [blockView1 setSelected:NO];
    [blockView2 setSelected:YES];
    

    
    [UIView animateWithDuration:0.5 delay:0.0 options:0 animations:^{
         // Animate the alpha value of your imageView from 1.0 to 0.0 here
         blockView1.alpha = 0.0f;
     } completion:^(BOOL finished) {
         // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
         blockView1.hidden = YES;
         [blockView1 removeFromSuperview];
         [UIView animateWithDuration:0.5 delay:0.0 options:0 animations:^{
            [blockView2 setFrame:_frameCenter];
          } completion:^(BOOL finished) {
          }];
         
     }];
    NSDictionary *data=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithUnsignedLong:(unsigned long)2],@"vote", nil];
    runDelegate(_delegate, @selector(processFeedBack:),&data,nil);
    
}



@end
