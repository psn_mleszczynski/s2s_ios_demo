//
//  SimpleRate.m
//  SP2app
//
//  Created by Cedric Hamelin on 29/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

// THIS IS ONLY EXAMPLE CODE
/* You may use this code as an example to get you inspired of how to display the payload for a QUIZZ widget */


#import "SimpleQuizz.h"

@implementation SimpleQuizz

+ (id)JSONObjectWithData:(NSData *)data {
    Class jsonSerializationClass = NSClassFromString(@"NSJSONSerialization");
    if (!jsonSerializationClass) {
        //iOS < 5 didn't have the JSON serialization class
        return [data objectFromJSONData]; //JSONKit
    }
    else {
        NSError *jsonParsingError = nil;
        id jsonObject = [jsonSerializationClass JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
        return jsonObject;
    }
    return nil;
}


-(id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate andWithComment:(NSString*)comment andWithImage:(UIImage *)image andWithChoices:(NSArray *)choices
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame=frame;
        //NSLog(@"rate > (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
        _w = frame.size.width;
        _h = frame.size.height;
        _x = frame.origin.x;
        _y = frame.origin.y;
        _delegate = delegate;
            
        // Initialization code
        int _imgh = _h-70;              //image heigth
        int _imgw = _w*5/12-20;          //_imgh*3/4;          //image width
        int _tw=_w-40;                  //title ctrl width

        // QUIZZ TITLE
        //UILabel *question = [[UILabel alloc] initWithFrame:CGRectMake((_w -_tw)/2, 10, _tw, 42)];
        UILabel *question = [[UILabel alloc] initWithFrame:CGRectMake(0,_h-45,_w-30,30)];
        //[title setCenter:CGPointMake((_w + _imgw)/2-150,25)];
        [question setTextAlignment:NSTextAlignmentCenter];
        [question setText:comment];
        [question setTextColor:[UIColor whiteColor]];
        [question setBackgroundColor:[UIColor clearColor]];
        [question setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:32.0f]];
        //title.layer.borderColor = [UIColor redColor].CGColor;
        //title.layer.borderWidth = 2.0;
        
        
        //description.layer.borderColor = [UIColor redColor].CGColor;
        //description.layer.borderWidth = 2.0;
        
        // IMAGE LEFT
        UIImageView* blockView = [[UIImageView alloc] initWithImage:image];
        blockView.contentMode = UIViewContentModeScaleAspectFit;//UIViewContentModeScaleAspectFill;
        //blockView.frame = CGRectMake(_w/2-_imgw/2,45,_imgw,_imgh);
        blockView.frame = CGRectMake(20,10,_imgw,_imgh);
        //blockView.layer.borderColor = [UIColor greenColor].CGColor;
        //blockView.layer.borderWidth = 2.0;
        
        
        
        
        // QUESTION BLOCS
        buttons = [NSMutableArray arrayWithCapacity:[choices count]];
        int i=0;
        for(NSString *q in choices) {
            //NSLog(@"quizz > create button for %@",q);
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(_w/2,60*i+10,_w/2-20,40)];
            [btn setTag:i];
            i++;
            btn.titleLabel.text = [NSString stringWithFormat:@"%@",q];
            [btn setTitle:btn.titleLabel.text forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(makeChoice:) forControlEvents:UIControlEventTouchUpInside];
            [btn setBackgroundColor:[UIColor whiteColor]];
            [btn.layer setBorderColor:[UIColor grayColor].CGColor];
            [btn.layer setBorderWidth:1.0];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            [btn setBackgroundImage:[SimpleQuizz imageWithColor:[UIColor redColor]] forState:UIControlStateSelected];
            btn.enabled = YES;
            [self addSubview:btn];
            [buttons addObject:btn];
        }
        
        
        [self addSubview:question];
        [self addSubview:blockView];
        
    }
    return self;
}

+(UIImage *)imageWithColor:(UIColor *)color {
   CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
   UIGraphicsBeginImageContext(rect.size);
   CGContextRef context = UIGraphicsGetCurrentContext();

   CGContextSetFillColorWithColor(context, [color CGColor]);
   CGContextFillRect(context, rect);

   UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();

   return image;
}

+(UIImage *)barWithColor:(UIColor *)color untilPercent:(float)pc andBgColor:(UIColor *)bgcolor {
   CGRect bgRect = CGRectMake(0.0f, 0.0f, 100.0f, 1.0f);
   CGRect barRect = CGRectMake(0.0f, 0.0f, 100*pc, 1.0f);
   
   UIGraphicsBeginImageContext(bgRect.size);
   CGContextRef context = UIGraphicsGetCurrentContext();

   CGContextSetFillColorWithColor(context, [bgcolor CGColor]);
   CGContextFillRect(context, bgRect);
   
   CGContextSetFillColorWithColor(context, [color CGColor]);
   CGContextFillRect(context, barRect);

   UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();

   return image;
}



-(void)makeChoice:(id)sender {
    int tag = [(UIButton*)sender tag];
    NSLog(@"quizz > you choose (%d)",tag);
    
    for (UIButton* btn in buttons) {
        if (btn != sender) {
             [btn setSelected: FALSE];
             [btn setBackgroundColor:[UIColor blackColor]];
        }
        if (btn == sender) {
             [btn setSelected: TRUE];
             [btn setBackgroundColor:[UIColor redColor]];
        }
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setEnabled:NO];
    }
    
    NSDictionary *data=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithUnsignedLong:(unsigned long)tag],@"quizz", nil];
    
    // If we want to process feedback results we need to retain (CFRetain) the pointer to &results and transfer (__bridge_transfer) ownership here.
    // otherwise the app will crash.
    CFTypeRef result;
    if ([_delegate respondsToSelector:@selector(processFeedBack:)]) {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[[_delegate class] instanceMethodSignatureForSelector:@selector(processFeedBack:)]];
        [invocation setSelector:@selector(processFeedBack:)];
        [invocation setTarget:_delegate];
        [invocation setArgument:&data atIndex:2];
        [invocation invoke];
        [invocation getReturnValue:&result];
        if (result)
            CFRetain(result);
    }
    NSMutableDictionary *rs = (__bridge_transfer NSMutableDictionary *)result;
    NSLog(@"quizz > %@",[rs valueForKeyPath:@"stats.widgetStats.stats.response"]);
    
    for (UIButton* btn in buttons) {
        //+(UIImage *)barWithColor:(UIColor *)color untilPercent:(float)pc andBgColor:(UIColor *)bgcolor
        float pc = 0.01*[[[rs valueForKeyPath:@"stats.widgetStats.stats.response"] objectForKey:[NSString stringWithFormat:@"%d",[btn tag]]] floatValue];
        NSLog(@"quizz > response [%@ -> %.2f]",[NSNumber numberWithInt:[btn tag]],pc);
        UIImage *bg = [SimpleQuizz barWithColor:[UIColor greenColor] untilPercent:pc andBgColor:[UIColor blackColor]];

        [btn setBackgroundColor:[UIColor clearColor]];
        [btn setBackgroundImage:bg forState:UIControlStateDisabled];
        [btn setBackgroundImage:bg forState:UIControlStateNormal];
        [btn setBackgroundImage:bg forState:UIControlStateSelected];
    }
    
    /*
{
    data =     {
        quizz = 0;
    };
    first = 1;
    stats =     {
        widgetStats =         {
            header =             {
                now = "1394543074.267030001";
            };
            id = 41192408858889806653;
            stats =             {
                "first_response" =                 {
                    deviceId = 84145cad5ec471dab05a2cefdcf3ce3e9eaee26d;
                    response = 1;
                    time = "20140311 11:41:29.713570";
                };
                response =                 {
                    0 = "46.15";
                    1 = "42.31";
                    2 = "11.54";
                };
            };
        };
    };
    status = OK;
}*/
    
    
    
    /*
    if ([(UIButton*)sender isSelected] == TRUE) {
        [(UIButton*)sender setSelected:FALSE];
        [(UIButton*)sender setBackgroundColor:[UIColor clearColor]];
    }
    else if ([(UIButton*)sender isSelected] == FALSE) {
        [(UIButton*)sender setSelected:TRUE];
        [(UIButton*)sender setBackgroundColor:[UIColor redColor]];
    }*/
    
}

@end
