//
//  SimpleRate.m
//  SP2app
//
//  Created by Cedric Hamelin on 29/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//


// THIS IS ONLY EXAMPLE CODE
/* You may use this code as an example to get you inspired of how to display the payload for a RATE widget */


#import "SimpleRate.h"

@implementation SimpleRate


- (id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate
                                  andWithTitle:(NSString *)titleText
                                  andWithDescriptionText:(NSString*)descriptionText
                                  andWithImage:(UIImage *)image
    
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame=frame;
        //NSLog(@"rate > (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
        _w = frame.size.width;
        _h = frame.size.height;
        _x = frame.origin.x;
        _y = frame.origin.y;
        _delegate = delegate;
            
        // Initialization code
        int _imgh = _h-20;              //image heigth
        int _imgw = _imgh*3/4;          //image width
        int _rcw=(_w*4)/10;                   //rating ctrl width
        

        
        // RATING TITLE
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake((_w + _imgw -_rcw)/2+10, 25, _rcw, 42)];
        //[title setCenter:CGPointMake((_w + _imgw)/2-150,25)];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setText:titleText];
        [title setTextColor:[UIColor whiteColor]];
        [title setBackgroundColor:[UIColor clearColor]];
        //[title setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.8]];
        [title setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:32.0f]];
        //title.layer.borderColor = [UIColor redColor].CGColor;
        //title.layer.borderWidth = 2.0;
        
        //UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake((_w + _imgw -_rcw)/2+10, 120, _rcw, _h-130)];
        UITextView *description = [[UITextView alloc] initWithFrame:CGRectMake((_w + _imgw -_rcw)/2+_w/20, 120, _rcw, _h-130)];
        
        //[title setTextAlignment:NSTextAlignmentCenter];
        //description.lineBreakMode = NSLineBreakByWordWrapping;
        //[description sizeToFit];
        [description setText:descriptionText];
        [description setTextColor:[UIColor whiteColor]];
        [description setBackgroundColor:[UIColor clearColor]];
        [description setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f]];
        
        [description setEditable:NO];
        
        
        //description.layer.borderColor = [UIColor redColor].CGColor;
        //description.layer.borderWidth = 2.0;
        
        // RATING STARS
        starRatingControl = [[StarRatingControl alloc] initWithFrame:CGRectMake( (_w + _imgw - _rcw)/2 +10 ,65,_rcw,50) andStars:5];
        starRatingControl.delegate = self;
        [starRatingControl setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.1]];
        //starRatingControl.layer.borderColor = [UIColor redColor].CGColor;
        //starRatingControl.layer.borderWidth = 2.0;
    
        // IMAGE
        //UIImage* image = [UIImage imageNamed:[NSString stringWithFormat:@"Images/Blocks/%@block.jpg", colour]];
        UIImageView* blockView = [[UIImageView alloc] initWithImage:image];
        blockView.contentMode = UIViewContentModeScaleAspectFit;//UIViewContentModeScaleAspectFill;
        //blockView.frame = CGRectMake(_w/2-_imgw/2,45,_imgw,_imgh);
        blockView.frame = CGRectMake(_w/20,10,_imgw,_imgh);
        //blockView.layer.borderColor = [UIColor greenColor].CGColor;
        //blockView.layer.borderWidth = 2.0;
        
        [self addSubview:title];
        [self addSubview:description];
        [self addSubview:blockView];
        [self addSubview:starRatingControl];
        
        //self.layer.borderColor = [UIColor yellowColor].CGColor;
        //self.layer.borderWidth = 2.0;

    }
    return self;
}

- (void)starRatingControl:(StarRatingControl *)control willUpdateRating:(NSUInteger)rating {
	//ratingLabel.text = [_ratingLabels objectAtIndex:rating];
    //NSLog(@"rate > value %lu",(unsigned long)rating);
}
- (void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating {
	// Call back to indicate the control is currently being updated
    NSLog(@"rate > value %lu",(unsigned long)rating);
    NSDictionary *data=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithUnsignedLong:(unsigned long)rating],@"rating", nil];
    runDelegate(_delegate, @selector(processFeedBack:),&data,nil);

}
 
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
