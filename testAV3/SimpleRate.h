//
//  SimpleRate.h
//  SP2app
//
//  Created by Cedric Hamelin on 29/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarRatingControl.h"

#define runDelegate(clss,sel,input,output) {\
if (!clss) {\
NSLog(@"run > ERROR, %@ class not found!",[[clss class] description]);\
}\
else if ([clss respondsToSelector:sel]) {\
NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:\
[[clss class] instanceMethodSignatureForSelector:sel]];\
[invocation setSelector:sel];\
[invocation setTarget:clss];\
if (input!=nil) {\
[invocation setArgument:input atIndex:2];\
}\
[invocation invoke];\
if (output!=nil) {\
[invocation getReturnValue:output];\
}\
}\
}\



@interface SimpleRate : UIView <StarRatingDelegate> {
    int _w,_h,_x,_y;
    id _delegate;
    
    NSArray *_ratingLabels;
    UILabel *ratingLabel;
    StarRatingControl *starRatingControl;
}
-(id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate andWithTitle:(NSString *)title andWithDescriptionText:(NSString*)descriptionText andWithImage:(UIImage *)image;

-(void)drawHorizontal;
-(void)drawVertical;

@end
