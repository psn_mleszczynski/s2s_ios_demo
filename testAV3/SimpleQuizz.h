//
//  SimpleRate.h
//  SP2app
//
//  Created by Cedric Hamelin on 29/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Filters.h"
#import "JSONKit.h"


@interface SimpleQuizz: UIView {
    int _w,_h,_x,_y;
    id _delegate;
    
    NSMutableArray* buttons;
}



-(id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate andWithComment:(NSString*)comment andWithImage:(UIImage *)image andWithChoices:(NSArray *)choices;
-(void)drawHorizontal;
-(void)drawVertical;
+(UIImage *)imageWithColor:(UIColor *)color;
+(UIImage *)barWithColor:(UIColor *)color untilPercent:(float)pc andBgColor:(UIColor *)bgcolor;

@end
