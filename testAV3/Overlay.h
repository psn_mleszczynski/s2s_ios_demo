//
//  Overlay.h
//  testAV3
//
//  Created by Cedric Hamelin on 28/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Overlay : UIView {
    int _w,_h,_x,_y;
    
    // display items
    UIImageView *statusImage;
    
}
-(void)displayStatus:(NSDictionary*)status;
-(void)drawHorizontal;
-(void)drawVertical;

@end
