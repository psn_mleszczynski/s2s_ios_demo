//
//  Overlay.m
//  testAV3
//
//  Created by Cedric Hamelin on 28/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import "Overlay.h"

@implementation Overlay

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code
        self.frame=frame;
        NSLog(@"overlay > (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);        
        _w = frame.size.width;
        _h = frame.size.height;
        _x = frame.origin.x;
        _y = frame.origin.y;


        self.center = CGPointMake(_w/2,_h/2);

        [self displayStatus:nil];
        
        /*self.layer.borderColor = [UIColor redColor].CGColor;
        self.layer.borderWidth = 3.0f;*/

        self.userInteractionEnabled = YES;
    }
    return self;
}


-(void)displayStatus:(NSDictionary*)status {
    if (!statusImage) {
        statusImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-bw.png"]];
        CGFloat img_h = statusImage.frame.size.height;
        CGFloat img_w = statusImage.frame.size.width;
        statusImage.frame = CGRectMake(_x+_w/2-img_w/2,_y+_h/2-img_h/2, img_w, img_h);
        [self addSubview:statusImage];
    }
    if (!status) return;
    if ([[status allKeys] containsObject:@"s2s"]) {
        if ([[status objectForKey:@"s2s"] isEqualToString:@"on"]) {
            [statusImage setImage:[UIImage imageNamed:@"icon-m.png"]];
        } else {
            [statusImage setImage:[UIImage imageNamed:@"icon-bw.png"]];
        }
    }
}

-(void)drawHorizontal {
    NSLog(@"overlay > drawHorizontal (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
    self.frame = CGRectMake(0,0,_w,_h);
    CGFloat img_h = statusImage.frame.size.height;
    CGFloat img_w = statusImage.frame.size.width;
    statusImage.frame = CGRectMake(_x+_w/2-img_w/2,_y+_h/2-img_h/2, img_w, img_h);
}

-(void)drawVertical {
    NSLog(@"overlay > drawVertical (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);        
    self.frame = CGRectMake(0,0,_h,_w);
    CGFloat img_h = statusImage.frame.size.height;
    CGFloat img_w = statusImage.frame.size.width;
    statusImage.frame = CGRectMake(_y+_h/2-img_w/2,_x+_w/2-img_h/2, img_w, img_h);
}

@end
