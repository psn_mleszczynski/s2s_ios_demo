//
//  main.m
//  testAV3
//
//  Created by Cedric Hamelin on 28/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
