//
//  SimpleC2A.h
//  SP2app
//
//  Created by Cedric Hamelin on 11/02/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Filters.h"
#import "SSZipArchive.h"

#define runDelegate(clss,sel,input,output) {\
if (!clss) {\
NSLog(@"run > ERROR, %@ class not found!",[[clss class] description]);\
}\
else if ([clss respondsToSelector:sel]) {\
NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:\
[[clss class] instanceMethodSignatureForSelector:sel]];\
[invocation setSelector:sel];\
[invocation setTarget:clss];\
if (input!=nil) {\
[invocation setArgument:input atIndex:2];\
}\
[invocation invoke];\
if (output!=nil) {\
[invocation getReturnValue:output];\
}\
}\
}\

@interface SimpleC2A : UIView <UIWebViewDelegate> {
    int _w,_h,_x,_y;
    id _delegate;
    
    //UIButton *blockView;
    NSString *_url;
    UIWebView *_wv;
    UIWebView *_anim;

}

-(id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate andWithTitle:(NSString *)title andWithZip:(NSString *)b64zip andWithURL:(NSString *)url;

-(void)showWebView;

-(void)drawHorizontal;
-(void)drawVertical;
@end
