//
//  SimpleC2A.m
//  SP2app
//
//  Created by Cedric Hamelin on 11/02/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

// THIS IS ONLY EXAMPLE CODE
/* You may use this code as an example to get you inspired of how to display the payload for a CHECKIN widget */



#import "SimpleCheckIn.h"



@implementation SimpleCheckIn


+ (id)JSONObjectWithData:(NSData *)data {
    Class jsonSerializationClass = NSClassFromString(@"NSJSONSerialization");
    if (!jsonSerializationClass) {
        //iOS < 5 didn't have the JSON serialization class
        return [data objectFromJSONData]; //JSONKit
    }
    else {
        NSError *jsonParsingError = nil;
        id jsonObject = [jsonSerializationClass JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
        return jsonObject;
    }
    return nil;
}


- (id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate
                                  andWithChannel:(NSString *)channel
                                  andWithImage:(UIImage *)image
                                  andWithTrigger:(NSDictionary *)trig
    
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame=frame;
        //NSLog(@"rate > (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
        _w = frame.size.width;
        _h = frame.size.height;
        _x = frame.origin.x;
        _y = frame.origin.y;
        _delegate = delegate;
            
        // Initialization code
        _imgh = _h-52;                      //image heigth
        _imgw = MIN(_imgh*1.30,_w*0.7);     //image width

        // NSString*channel may be something like "[2]france-2"     "2" is the ID used by ACR engine while "france-2" is the ID used by the EPG service
        NSRange start = [channel rangeOfString:@"["];
        NSRange end = [channel rangeOfString:@"]"];
        if (start.location != NSNotFound && end.location != NSNotFound && end.location > start.location) {
            NSString *subID = [channel substringWithRange:NSMakeRange(start.location+1, end.location-(start.location+1))];
            channel = [channel substringWithRange:NSMakeRange(end.location+1, channel.length-(end.location+1))];
        } else {
        }

        _channelName = [NSString stringWithFormat:@"%@",channel];

        double time = [[trig objectForKey:@"timestamp"] doubleValue];

        
        
        
        NSString *channelid = [trig objectForKey:@"id"];
        //[[url stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""];//[NSString stringWithFormat:@"%@",url];

 
        
        // RATING TITLE
        //title = [[UILabel alloc] initWithFrame:CGRectMake((_w + _imgw -_rcw)/2+10, 25, _rcw, 42)];
        title = [[UILabel alloc] initWithFrame:CGRectMake(0,0,_w,42)];
        
        //[title setCenter:CGPointMake((_w + _imgw)/2-150,25)];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setText:@"checking in..."];
        [title setTextColor:[UIColor whiteColor]];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:32.0f]];
        //title.layer.borderColor = [UIColor redColor].CGColor;
        //title.layer.borderWidth = 2.0;
        
        
        // IMAGE
        //UIImage* image = [UIImage imageNamed:[NSString stringWithFormat:@"Images/Blocks/%@block.jpg", colour]];
        //UIImageView* blockView = [[UIImageView alloc] initWithImage:image];
        //blockView.contentMode = UIViewContentModeScaleAspectFit;//UIViewContentModeScaleAspectFill;
        //blockView.frame = CGRectMake(_w/2-_imgw/2,45,_imgw,_imgh);
        //blockView.frame = CGRectMake(20,10,_imgw,_imgh);
        //blockView.layer.borderColor = [UIColor greenColor].CGColor;
        //blockView.layer.borderWidth = 2.0;
        
        UIImage *imON1;
        UIImage *imOFF1;

        imON1 = [image copy];
        imOFF1 = [[image copy] saturateImage:0.0 withContrast:1.0];
        
        //blockView = [[UIButton alloc] initWithFrame:CGRectMake(0,0,_w,_h)];
        /*blockView = [[UIButton alloc] initWithFrame:CGRectMake(0,52,_imgw,_imgh)];
        blockView.contentMode = UIViewContentModeScaleAspectFit;
        [blockView setImage:imON1 forState:UIControlStateNormal];
        [blockView setImage:imON1 forState:UIControlStateSelected];
        [blockView setImage:imON1 forState:UIControlStateHighlighted];
        [blockView setImage:imOFF1 forState:UIControlStateDisabled];
        [blockView setAdjustsImageWhenDisabled:NO];
        blockView.contentMode=UIViewContentModeScaleAspectFit;*/
        
        //blockView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,_w,_h)];
        blockView = [[UIImageView alloc] initWithImage:imOFF1];
        blockView.frame = CGRectMake(0,52,_imgw,_imgh);
        blockView.contentMode = UIViewContentModeScaleAspectFit;

        //[blockView addTarget:self action:@selector(launchFacebookController) forControlEvents:UIControlEventTouchUpInside];
        [blockView setAlpha:1.0f];
        //blockView.layer.borderColor = [UIColor greenColor].CGColor;
        //blockView.layer.borderWidth = 2.0;
        //[blockView setEnabled:NO];
        [self addSubview:title];
        [self addSubview:blockView];
        
        
        if (time>0.0) {
            _url = [NSString stringWithFormat:@"http://s2s-app.appspot.com/epg?channel=%@&time=%.2f",channel,time];
        } else {
            _url = [NSString stringWithFormat:@"http://s2s-app.appspot.com/epg?channel=%@",channel];    // will check EPG for channel now
        }

        NSLog(@"checkin > getting EPG data from %@",_url);
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_url]];

        __block NSDictionary *json;
        
        /* JSON DATA FROM EPG GUIDE WILL BE SOMETHING LIKE THIS:
        {
            header =     {
                channel = "france-2";
                time = "2014-02-20T09:04:16.100000+01:00";
            };
            data =     {
                "france-2" =         {
                    duration = "0:55:00";
                    start = "2014-02-20T08:10:00";
                    stop = "2014-02-20T09:05:00";
                    title = "T\U00e9l\U00e9matin (suite)";
                    url = "/tele/programmes-tv/telematin-suite,74103199.php";
                };
            };
        }
        */
        
        [NSURLConnection sendAsynchronousRequest:request
                         queue:[NSOperationQueue mainQueue]
                         completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                if (data!=nil) {
                         
                                    json = [NSJSONSerialization JSONObjectWithData:data
                                                                options:0
                                                                error:nil];
                                    NSLog(@"checkin > epg data: %@", json);

                                    @try {
                                        _prgmName = [[[json objectForKey:@"data"] objectForKey:channel] objectForKey:@"title"];
                                        NSLog(@"checkin > program title: %@",_prgmName);
                                    }
                                    @catch (NSException *e) {
                                        NSLog(@"checkin > there has been a problem with EPG data");
                                        _prgmName = @"could not find epg data";

                                    }
                                    [title setText:_prgmName];
                                 
                                    blockView.image = imON1;
                                    //[blockView setEnabled:YES];
                                 
                                 
                                    // CREATE FACEBOOK BUTTON
                                    UIImage *imFBON1;
                                    UIImage *imFBOFF1;

                                    imFBON1 = [UIImage imageNamed:@"facebook_button_small.png"];
                                    imFBOFF1 = [[imFBON1 copy] saturateImage:0.0 withContrast:1.0];
                                    
                                    UIButton *fbButton = [[UIButton alloc] initWithFrame:CGRectMake(_imgw+15,52,(_w-_imgw-20),(_w-_imgw-20)*57/250)];
                                    fbButton.contentMode = UIViewContentModeScaleAspectFit;
                                    [fbButton setImage:imFBON1 forState:UIControlStateNormal];
                                    [fbButton setImage:imFBOFF1 forState:UIControlStateSelected];
                                    [fbButton setImage:imFBOFF1 forState:UIControlStateHighlighted];
                                    [fbButton setImage:imFBOFF1 forState:UIControlStateDisabled];
                                    [fbButton setAdjustsImageWhenDisabled:NO];
                                    fbButton.contentMode=UIViewContentModeScaleAspectFit;

                                    [fbButton addTarget:self action:@selector(launchFacebookController) forControlEvents:UIControlEventTouchUpInside];
                                    [fbButton setAlpha:1.0f];

                                    
                                    [fbButton setEnabled:YES];
                                    [self addSubview:fbButton];


                                    // CREATE TWITTER BUTTON
                                    UIImage *imTWON1;
                                    UIImage *imTWOFF1;

                                    imTWON1 = [UIImage imageNamed:@"twitter_button_small.png"];
                                    imTWOFF1 = [[imTWON1 copy] saturateImage:0.0 withContrast:1.0];
                                 
                                    UIButton *twButton = [[UIButton alloc] initWithFrame:CGRectMake(_imgw+15,72+(_w-_imgw-20)*57/250,(_w-_imgw-20),(_w-_imgw-20)*57/250)];
                                    twButton.contentMode = UIViewContentModeScaleAspectFit;
                                    [twButton setImage:imTWON1 forState:UIControlStateNormal];
                                    [twButton setImage:imTWOFF1 forState:UIControlStateSelected];
                                    [twButton setImage:imTWOFF1 forState:UIControlStateHighlighted];
                                    [twButton setImage:imTWOFF1 forState:UIControlStateDisabled];
                                    [twButton setAdjustsImageWhenDisabled:NO];
                                    twButton.contentMode=UIViewContentModeScaleAspectFit;

                                    [twButton addTarget:self action:@selector(launchTwitterController) forControlEvents:UIControlEventTouchUpInside];
                                    [twButton setAlpha:1.0f];

                                    [twButton setEnabled:YES];
                                    [self addSubview:twButton];
                                 
                                 
                                    @try {
                                        if ([[[[json objectForKey:@"data"] objectForKey:channel] objectForKey:@"details"] objectForKey:@"img"]) {
                                            NSURL *url = [NSURL URLWithString:[[[[json objectForKey:@"data"] objectForKey:channel] objectForKey:@"details"] objectForKey:@"img"]];
                                            NSLog(@"checkin > program image: %@",url);
                                            NSData *data = [NSData dataWithContentsOfURL:url];
                                            UIImage *img = [[UIImage alloc] initWithData:data];
                                            
                                            UIImage *imON1;
                                            UIImage *imOFF1;

                                            imON1 = [img copy];
                                            imOFF1 = [[img copy] saturateImage:0.0 withContrast:1.0];
                                            /*[blockView setImage:imON1 forState:UIControlStateNormal];
                                            [blockView setImage:imON1 forState:UIControlStateSelected];
                                            [blockView setImage:imON1 forState:UIControlStateHighlighted];
                                            [blockView setImage:imOFF1 forState:UIControlStateDisabled];*/
                                            
                                            blockView.image = imON1;
                                            
                                        }
                                    }
                                    @catch (NSException *e) {
                                    }
                                }
                             
                       }];
        
        
        
        
        /*
        // WEBVIEW
        _wv = [[UIWebView alloc] initWithFrame:CGRectMake(0,0,_w,_h)];
        _wv.delegate = self;
        _wv.multipleTouchEnabled = YES;
        _wv.scalesPageToFit = NO;
        [_wv setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        
        // Prevent page from bouncing
        for (id subview in _wv.subviews)
            if ([[subview class] isSubclassOfClass: [UIScrollView class]])
                ((UIScrollView *)subview).bounces = NO;
        
        NSURL* nsUrl = [NSURL URLWithString:_url];
        //NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
        NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
        [_wv loadRequest:request];
        
        [_wv setAllowsInlineMediaPlayback:YES];
        [_wv setDataDetectorTypes:UIDataDetectorTypeLink];
        [_wv setBackgroundColor: [UIColor colorWithRed:0. green:0.39 blue:0.106 alpha:0.]]; //[UIColor clearColor]]; //[UIColor blackColor]]; // // darkGrayColor [UIColor blackColor]]; //[UIColor clearColor]];
        [_wv setOpaque:NO];

        NSLog(@"checkin > loading page : [%@]",_url);
        _wv.hidden = YES;
        
        [self addSubview:_wv];*/
        
        
    }
    return self;
}

/*
-(void)showWebView {
    _wv.hidden=NO;
}
*/

-(void)drawHorizontal {
        NSLog(@"vote > drawHorizontal");
        int _imgh = _h-70;              //image heigth
        int _imgw = _w/2-20;          //_imgh*3/4;          //image width
        int _tw=_w-40;                  //title ctrl width
    
}

-(void)drawVertical {
        NSLog(@"vote > drawVertical");
        int _imgh = _h-70;              //image heigth
        int _imgw = _w/2-20;          //_imgh*3/4;          //image width
        int _tw=_h-20;                  //title ctrl width
    

}

-(void)launchFacebookController{
 
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
 
        NSString *message = [NSString stringWithFormat:@"I enjoyed watching %@ on channel %@\n",_prgmName,_channelName];
 
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:message];
        [controller addURL:[NSURL URLWithString:@"http://www.challenge2media.com"]];
        //[controller addImage:[blockView imageForState:UIControlStateNormal]];
        [controller addImage:blockView.image];

        [ROOTVIEW presentViewController:controller animated:YES completion:Nil];
 
        controller.completionHandler = ^(SLComposeViewControllerResult result){
            NSString *output= nil;
            NSDictionary *data;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output= @"Your post has been cancelled.";
                    NSLog (@"cancelled");
                    data=[NSDictionary dictionaryWithObjectsAndKeys:@"cancel",@"facebook", nil];
                    runDelegate(_delegate, @selector(processFeedBack:),&data,nil);
                    break;
                case SLComposeViewControllerResultDone:
                    output= @"Thank you for sharing !";
                    NSLog (@"success");
                    data=[NSDictionary dictionaryWithObjectsAndKeys:@"post",@"facebook", nil];
                    runDelegate(_delegate, @selector(processFeedBack:),&data,nil);
                    break;
                default:
                    break;
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
    }
}

-(void)launchTwitterController{

    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {

        NSString *message = [NSString stringWithFormat:@"I enjoyed watching %@ on channel %@\n",_prgmName,_channelName];

        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [controller setInitialText:message];
        [controller addURL:[NSURL URLWithString:@"http://www.challenge2media.com/"]]; // utilisez bit.ly pour raccourcir l'url
        //[controller addImage:[blockView imageForState:UIControlStateNormal]];
        [controller addImage:blockView.image];
        
        [ROOTVIEW presentViewController:controller animated:YES completion:Nil];

        controller.completionHandler = ^(SLComposeViewControllerResult result){
            NSString *output= nil;
            NSDictionary *data;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output= @"Your tweet has been cancelled.";
                    NSLog (@"cancelled");
                    data=[NSDictionary dictionaryWithObjectsAndKeys:@"cancel",@"twitter", nil];
                    runDelegate(_delegate, @selector(processFeedBack:),&data,nil);
                    break;
                case SLComposeViewControllerResultDone:
                    output= @"Thank you for your tweet.";
                    NSLog (@"success");
                    data=[NSDictionary dictionaryWithObjectsAndKeys:@"tweet",@"twitter", nil];
                    runDelegate(_delegate, @selector(processFeedBack:),&data,nil);
                    break;
                default:
                    break;
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
    }
}




-(void)webViewDidFinishLoad:(UIWebView *)webView {
    /*if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"]) {
        NSLog(@"c2a > page loaded : %@",_url);
    }*/
    NSString *status = [webView stringByEvaluatingJavaScriptFromString:@"document.readyState"];
    NSLog(@"c2a > page loaded : %@ (%@)",_url,status);
    //[webView stringByEvaluatingJavaScriptFromString:@"document.body.style.backgroundColor = 'rgba(255, 255, 255, 0.0)';"];
    
    
    if ([status isEqualToString:@"interactive"]) {
        //[blockView setEnabled:YES];
    }
    if ([status isEqualToString:@"complete"]) {
        //[blockView setEnabled:YES];
    }
    
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {

    /*NSLog(@"webview > full url     : %@", [request URL]);
    NSLog(@"webview > scheme       : %@", [[request URL] scheme]);
    NSLog(@"webview > host         : %@", [[request URL] host]);
    NSLog(@"webview > query        : %@", [[request URL] query]);
    NSLog(@"webview > fragment     : %@", [[request URL] fragment]);*/
    
    NSLog(@"c2a > preloading %@",[request URL]);
    
    return YES;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



@end
