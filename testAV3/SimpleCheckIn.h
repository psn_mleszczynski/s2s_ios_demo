//
//  SimpleC2A.h
//  SP2app
//
//  Created by Cedric Hamelin on 11/02/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "UIImage+Filters.h"
#import "JSONKit.h"

#define runDelegate(clss,sel,input,output) {\
if (!clss) {\
NSLog(@"run > ERROR, %@ class not found!",[[clss class] description]);\
}\
else if ([clss respondsToSelector:sel]) {\
NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:\
[[clss class] instanceMethodSignatureForSelector:sel]];\
[invocation setSelector:sel];\
[invocation setTarget:clss];\
if (input!=nil) {\
[invocation setArgument:input atIndex:2];\
}\
[invocation invoke];\
if (output!=nil) {\
[invocation getReturnValue:output];\
}\
}\
}\


#define ROOTVIEW [[[UIApplication sharedApplication] keyWindow] rootViewController]

//#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))


@interface SimpleCheckIn : UIView <UIWebViewDelegate> {
    int _w,_h,_x,_y;
    id _delegate;
    int _imgw,_imgh;
    
    UILabel *title;
    
    //UIButton *blockView;
    UIImageView *blockView;
    NSString *_url;
    NSString *_channelName;
    NSString *_prgmName;
    
    UIWebView *_wv;

}

-(id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate andWithChannel:(NSString *)title andWithImage:(UIImage *)image andWithTrigger:(NSDictionary *)trig;

-(void)showWebView;

-(void)drawHorizontal;
-(void)drawVertical;
@end
