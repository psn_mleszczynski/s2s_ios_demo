//
//  ViewController.m
//  testAV3
//
//  Created by Cedric Hamelin on 28/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Featured", @"Featured");
    }
    return self;
}
*/

-(BOOL)iOSVersionIsAtLeast:(NSString*)version {
    NSComparisonResult result = [[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch];
    return (result == NSOrderedDescending || result == NSOrderedSame);
}

- (CGRect)maximumUsableFrame {

    static CGFloat const kNavigationBarPortraitHeight = 44;
    static CGFloat const kNavigationBarLandscapeHeight = 34;
    static CGFloat const kToolBarHeight = 49;
    static CGFloat const kStatusBarHeight = 20;

    // Start with the screen size minus the status bar if present
    CGRect maxFrame = [UIScreen mainScreen].applicationFrame;

    // If the orientation is landscape left or landscape right then swap the width and height
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        fswap(&maxFrame.size.height,&maxFrame.size.width);
        /*CGFloat temp = maxFrame.size.height;
        maxFrame.size.height = maxFrame.size.width;
        maxFrame.size.width = temp;*/
    }

    // Take into account if there is a navigation bar present and visible (note that if the NavigationBar may
    // not be visible at this stage in the view controller's lifecycle.  If the NavigationBar is shown/hidden
    // in the loadView then this provides an accurate result.  If the NavigationBar is shown/hidden using the
    // navigationController:willShowViewController: delegate method then this will not be accurate until the
    // viewDidAppear method is called.
    if (self.navigationController) {
        if (self.navigationController.navigationBarHidden == NO) {

            // Depending upon the orientation reduce the height accordingly
            if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
                maxFrame.size.height -= kNavigationBarLandscapeHeight;
            }
            else {
                maxFrame.size.height -= kNavigationBarPortraitHeight;
            }
        }
    }

    if (([UIApplication sharedApplication].statusBarHidden)||YES) {
        if (![self iOSVersionIsAtLeast:@"7.1"]) {
            maxFrame.size.height += kStatusBarHeight;
        }
    }

    // Take into account if there is a toolbar present and visible
    /*if (self.tabBarController) {
        if (self.tabBarController.view.visible) maxFrame.size.height -= kToolBarHeight;
    }*/
    return maxFrame;
}


-(NSString*)getDeviceUDID {
    return [s2s getDeviceUDID];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [UIApplication sharedApplication].statusBarHidden=YES;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    _frame = [self maximumUsableFrame];
    
    /*UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        fswap(&_frame.size.width,&_frame.size.height);
    }*/
    
    _w = _frame.size.width;
    _h = _frame.size.height;
    _scrollStep = _w;

    self.view.frame = _frame;//CGRectMake(0,0,_w,_h);
    self.view.center = CGPointMake(_w/2,_h/2);

    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"bg14" ofType:@"mp4"]];

    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&err];
    [session setActive:YES error:&err];

    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc] initWithContentURL:url];
    mp.view.frame=CGRectMake(0.0,0.0,_w,_h);
    mp.view.bounds=CGRectMake(0.0,0.0,_w,_h);

    //[mp setUseApplicationAudioSession:NO];
    [mp setUseApplicationAudioSession:YES];
    [mp setControlStyle:MPMovieControlStyleNone]; //MPMovieControlStyleEmbedded
    //[mp setScalingMode:MPMovieScalingModeFill];
    //[mp setScalingMode:MPMovieScalingModeAspectFill];      // black bar below
    //[mp setScalingMode:MPMovieScalingModeAspectFit];       // black bars around
    [mp setScalingMode:MPMovieScalingModeFill];              // stretch no respect for aspect ratio
    [mp setFullscreen:NO];

    self->player = mp;
    [self->player setShouldAutoplay:NO];
    [self.view addSubview:self->player.view];



    _overlay=[[Overlay alloc]initWithFrame:CGRectMake(0.0,0.0,_w,_h)];
    _wp=[[WidgetPage alloc]initWithFrame:CGRectMake(_w,0.0,_w,_h)
                           andWithDelegate:self
                           andWithUDID:_udid
                           ];
    
   
    // Make the UIScrollView
    scroll = [[OverlayPager alloc] initWithFrame:CGRectMake(0.0,0.0,_w,_h)];
    [scroll addSubview:_wp];
    [scroll addSubview:_overlay];
    [scroll setContentSize:CGSizeMake(2*_w,_h)];

    
    
    // add the scroll view to your view
    [self->player.view addSubview:scroll];


    [self playVideoInLoopMode:YES];
}

-(void)viewDidAppear:(BOOL)animated {
	s2s = [[S2S alloc] initWithVC:self andWithSubscription:@"1"];
    [s2s setCtrlServer:@"37.187.141.60" withPort:85];
	[s2s setLicense:@"4yiixxi=8ARwMNV*Cmi3D23*MIRIecZ*8UARBcI*STCmWWDw46Ui42l*MIIMbac*0i6x2ii*SikCjnG*fMIIOcIeIAwMgwo="]; // THIS IS A TEMPORARY LICENSE GRANTED TO 'TDF' VALID UNTIL '2014/08/18' FOR DEV PURPOSES.
    [s2s startSession:NULL withACR:@"tdf-live" autoStartACR:YES];

    _udid=[s2s getDeviceUDID];
}


// method to play the video background
- (void)playVideoInLoopMode:(BOOL)loop  {

   if (loop) {
       self->player.repeatMode = MPMovieRepeatModeOne;
   }
   
   [self->player prepareToPlay];
   [self->player setFullscreen:YES animated:YES];
   //[self->player pause];
   [self->player  setShouldAutoplay:YES];
   //[self->player play];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)willRotateToInterfaceOrientation: (UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration {

    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) {
        NSLog(@"main > rotating LANDSCAPE");
        self->player.view.frame=CGRectMake(0.0,0.0,_w,_h);
        self->player.view.bounds=CGRectMake(0.0,0.0,_w,_h);
        
        _scrollStep = _w;
        [self->scroll drawHorizontal];
    }
    
    if (orientation == UIInterfaceOrientationPortrait) {
        NSLog(@"main > rotating PORTRAIT");
        self->player.view.frame=CGRectMake(0.0,0.0,_h,_w);
        self->player.view.bounds=CGRectMake(0.0,0.0,_h,_w);
        
        _scrollStep = _h;
        [self->scroll drawVertical];
    }

}


/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃                                                                                                  ┃
  ┃ S2S CallBacks here                                                                               ┃
  ┃                                                                                                  ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/




/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ onStatusChange                                                                                   ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(void)onStatusChange:(NSDictionary *)statusdata {
    NSLog(@"main > onStatusChange: %@",statusdata);
    [_overlay displayStatus:statusdata];
    
    if ([[statusdata allKeys] containsObject:@"s2s"]) {
        if ([[statusdata objectForKey:@"s2s"] isEqualToString:@"on"]) {
            [self->player play];
        } else {
            [self->player pause];
        }
    }
    
}


/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ onZapp                                                                                           ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(void)onZapp:(NSDictionary *)data {
    NSLog(@"main > onZapp !");
    NSString    *videoChannel = [data objectForKey:@"id"];
     NSLog(@"main > we just changed channel id %@",videoChannel);
}


/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ onConfig                                                                                         ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(void)onConfig:(NSDictionary *)wcfg {
    NSLog(@"main > onConfig !");
    // write your own code to handle configuration received from ctrl-server
    id serverdata = [wcfg valueForKeyPath:@"widget.settings-code.app-servers"];

    if ([serverdata isKindOfClass:[NSDictionary class]]) {
        //only one app server
        NSLog(@"main > cfg app server : (%@) %@",
				[serverdata objectForKey:@"name"],
				[serverdata objectForKey:@"server"]);
    } else {
        //several app servers registered
        for (id srv in serverdata) {
            NSLog(@"main > cfg app server : (%@) %@",
				[srv objectForKey:@"name"],
				[srv objectForKey:@"server"]);
        }
    }
}


/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ onCommand                                                                                        ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(void)onCommand:(NSDictionary *)cmd {
    NSLog(@"main > onCommand !");
   // write your own code to handle any application command you like
   NSString *sys = [cmd objectForKey:@"system"];
   if ([sys isEqualToString:@"torchLight"]) {
      if ([[cmd objectForKey:@"execute"] isEqualToString:@"on"]) {
         // write here your own code to switch the flash light on.
         /* 
         …
         [device setFlashMode:AVCaptureFlashModeOn];
         */
      }
   }
}


/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ onWidgetStart                                                                                    ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(void)onWidgetStart:(NSDictionary *)wdata {
        NSLog(@"main > onWidgetStart !");
    
        /* Widget data can be accessed like this for example :
         
         NSString *myPayload = [[[wdata objectForKey:@"widget"]
						    objectForKey:@"native-code"]
						    objectForKey:@"my-payload"];
         
        */
    
        if ([self->_wp onWidgetStart:wdata withTrigger:[s2s getChannelInfo]]) {
            [UIView animateWithDuration:1.0 animations:^{
                scroll.contentOffset = CGPointMake(_scrollStep, 0);
            }];
        }
}

/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ onWidgetStop                                                                                     ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(void)onWidgetStop:(NSDictionary *)wdata {
       NSLog(@"main > onWidgetStop !");
        [UIView animateWithDuration:1.0 animations:^{
            scroll.contentOffset = CGPointMake(0, 0);
        }
        completion:^(BOOL finished) {
            if (finished) {
                [self->_wp onWidgetStop:wdata];
            }
        }
        ];

}

/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ wakeUpWidget                                                                                     ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(void)wakeUpWidget:(NSString*)widgetid {
    [s2s wakeUpWidget:widgetid];
}


-(void)onNotify:(NSDictionary *)wdata {
    // write your own code to handle any application Notification received
    // You may have your device (if an iPhone) vibrate for example
    NSLog(@"main > notifying !");

    // You may lightup an icon from somewhere in your app
    //int category = [[[wdata valueForKeyPath:@"notify"] objectForKey:@"cat"] intValue];
    //int item = 	   [[[wdata valueForKeyPath:@"notify"] objectForKey:@"item"] intValue];

    //[self lightUpIcon:item fromCategory:category];

    // You may show a message on your UI
    //NSString *msg = [[wdata valueForKeyPath:@"notify"] objectForKey:@"msg"];
}


@end
