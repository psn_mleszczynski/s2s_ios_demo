//
//  ViewController.h
//  testAV3
//
//  Created by Cedric Hamelin on 28/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVAudioSession.h>

#import "OverlayPager.h"
#import "Overlay.h"
#import "WidgetPage.h"
#import <S2S/S2S.h>


CGSize physicalPixelSizeOfScreen(UIScreen *s) {
    CGSize result = s.bounds.size;

    if ([s respondsToSelector: @selector(scale)]) {
        CGFloat scale = s.scale;
        result = CGSizeMake(result.width * scale, result.height * scale);
    }

    return result;
}

@class S2S;
@interface ViewController : MPMoviePlayerViewController <UIGestureRecognizerDelegate> {
    MPMoviePlayerController *player;
    int _w,_h,_scrollStep;
    NSString *_udid;
    CGRect _frame;
    
    
    
    UIView *one;
    UIView *two;
    
    UISwipeGestureRecognizer *swipeRight;
    UISwipeGestureRecognizer *swipeLeft;
    
    OverlayPager *scroll;
    Overlay *_overlay;
    WidgetPage *_wp;
    
    // -------- S2S --------
    S2S *s2s;
}

-(NSString*)getDeviceUDID;

// CALL BACK FUNCTIONS (called by S2S)   -------------------
-(void)onStatusChange:(NSDictionary *)statusdata;
-(void)onConfig:(NSDictionary *)wcfg;
-(void)onNotify:(NSDictionary *)wdata;
-(void)onCommand:(NSDictionary *)cmd;
-(void)onConfidence:(NSString*)confidence;
-(void)onWidgetStart:(NSDictionary *)wdata;
-(void)onChannelInfo:(NSDictionary *)data;
-(void)onZapp:(NSDictionary *)data;

// MISCELANEOUS   ------------------------------------------
-(NSDictionary *)getChannelInfo;

@end
