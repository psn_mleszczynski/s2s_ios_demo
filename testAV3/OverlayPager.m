//
//  OverlayPager.m
//  SP2app
//
//  Created by Cedric Hamelin on 29/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import "OverlayPager.h"

@implementation OverlayPager

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _w = frame.size.width;
        _h = frame.size.height;
        _x = frame.origin.x;
        _y = frame.origin.y;
    
        // Initialization code
        [self setPagingEnabled:YES];
        //[self setDecelerationRate:UIScrollViewDecelerationRateFast];
        [self setDecelerationRate:UIScrollViewDecelerationRateNormal];
        [self setIndicatorStyle:UIScrollViewIndicatorStyleBlack];
        [self setMultipleTouchEnabled:YES];
        [self setBounces:NO];
        [self setShowsHorizontalScrollIndicator:NO];
        [self setCanCancelContentTouches:YES];
        [self setDelaysContentTouches:YES];
        //[scroll setUserInteractionEnabled:NO];
        [self setUserInteractionEnabled:YES];
        
        /*self.layer.borderColor = [UIColor redColor].CGColor;
        self.layer.borderWidth = 3.0f;*/
    }
    return self;
}


-(void)drawHorizontal {
        NSLog(@"scroll > drawHorizontal");
        self.frame=CGRectMake(0.0,0.0,_w,_h);
        [self setContentSize:CGSizeMake(2*_w,_h)];
        for(id w in self.subviews) {
            if ([w respondsToSelector:@selector(drawHorizontal)]) {
                [w drawHorizontal];
            }
        }
}

-(void)drawVertical {
        NSLog(@"scroll > drawVertical");
        self.frame=CGRectMake(0.0,0.0,_h,_w);
        [self setContentSize:CGSizeMake(2*_h,_w)];
        for(id w in self.subviews) {
            if ([w respondsToSelector:@selector(drawVertical)]) {
                [w drawVertical];
            }
        }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat width = self.frame.size.width;
    currentPage = (self.contentOffset.x + (0.5f * width)) / width;
    NSLog(@"scroll > current page: %ld",(long)currentPage);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat width = self.frame.size.width;
    currentPage = (self.contentOffset.x + (0.5f * width)) / width;
    NSLog(@"scroll > current page: %ld",(long)currentPage);
}



/*-(void)willRotateToInterfaceOrientation: (UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration {

    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) {
        NSLog(@"widget > rotating LANDSCAPE");
    }
    
    if (orientation == UIInterfaceOrientationPortrait) {
        NSLog(@"widget > rotating PORTRAIT");
    }

}*/




@end
