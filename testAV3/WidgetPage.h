//
//  BW.h
//  testAV3
//
//  Created by Cedric Hamelin on 28/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Base64.h"


#import "SimpleRate.h"
#import "SimpleVote.h"
#import "SimpleC2A.h"
#import "SimpleCheckIn.h"
#import "SimpleQuizz.h"

@interface WidgetPage : UIView <UIWebViewDelegate> {
    int _w,_h,_x,_y,_mh,_mw,_index;
    id _delegate;
    
    UIToolbar *bg;  //iOS >= 7.0
    UIView *sbg;    //iOS < 7.0
    
    NSString *_udid;
    NSString *_wid;
    NSString *_feedbackUrl;
    
    NSString *title;
    
    UIWebView *webView;
    NSString *systemModel;
    NSString *systemName;
    NSString *systemVersion;
    
    CGRect _widgetFrame;
    
    BOOL isRetina;
    
    id widget;
    
}
-(id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate andWithUDID:(NSString *)udid;
-(NSDictionary*)processFeedBack:(NSDictionary*)fb;

// UI FUNCTIONS   ------------------------------------------
-(BOOL)onWidgetStart:(NSDictionary *)wdata withTrigger:(NSDictionary *)trig;
-(BOOL)onWidgetStop:(NSDictionary *)wdata;

-(BOOL)iOSVersionIsAtLeast:(NSString*)version;

@end

