//
//  SimpleC2A.m
//  SP2app
//
//  Created by Cedric Hamelin on 11/02/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

// THIS IS ONLY EXAMPLE CODE
/* You may use this code as an example to get you inspired of how to display the payload for a CALL2ACTION widget */

#import "SimpleC2A.h"

@implementation SimpleC2A

- (id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate
                                  andWithTitle:(NSString *)titleText
                                  andWithZip:(NSString *)b64zip
                                  andWithURL:(NSString *)url
    
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame=frame;
        //NSLog(@"rate > (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
        _w = frame.size.width;
        _h = frame.size.height;
        _x = frame.origin.x;
        _y = frame.origin.y;
        _delegate = delegate;
            
        // Initialization code
        int _imgh = _h-20;              //image heigth
        int _imgw = _imgh*3/4;          //image width
        int _rcw=250;                   //rating ctrl width
        
        _url = [[url stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""];//[NSString stringWithFormat:@"%@",url];
        
        
        // WEBVIEW FOR ANIMATION
        
        //1) decode base64 zip
        NSString * docsDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString * zipFile = [docsDir stringByAppendingPathComponent:@"anim.zip"];
        
        NSData *b64data=[[NSData alloc] initWithBase64Encoding:b64zip];
        [b64data writeToFile:zipFile atomically:YES];
        
        NSString * animFolder = [docsDir stringByAppendingPathComponent:@"anim"];
        
        //2) Unzipping
        [SSZipArchive unzipFileAtPath:zipFile toDestination:animFolder];

        NSString * indexHtml = [animFolder stringByAppendingPathComponent:@"index.html"];
        
        _anim = [[UIWebView alloc] initWithFrame:CGRectMake(0,0,_w,_h)];

        _anim.multipleTouchEnabled = NO;
        //[_anim setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        
        // Prevent page from bouncing
        for (id subview in _anim.subviews)
            if ([[subview class] isSubclassOfClass: [UIScrollView class]])
                ((UIScrollView *)subview).bounces = NO;
        
        NSURL *rootUrl = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:@"anim"]];
        rootUrl = [rootUrl URLByAppendingPathComponent:@"" isDirectory:TRUE];
        
        [_anim loadData:[NSData dataWithContentsOfFile:indexHtml] MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:rootUrl];
        [_anim scalesPageToFit];
        [_anim setContentMode:UIViewContentModeScaleAspectFit];
        

        
        [_anim setAllowsInlineMediaPlayback:YES];
        [_anim setDataDetectorTypes:UIDataDetectorTypeLink];
        //[_anim setBackgroundColor: [UIColor colorWithRed:0. green:0.39 blue:0.106 alpha:0.]]; //[UIColor clearColor]]; //[UIColor blackColor]]; // // darkGrayColor [UIColor blackColor]]; //[UIColor clearColor]];
        [_anim setBackgroundColor:[UIColor clearColor]];
        [_anim stringByEvaluatingJavaScriptFromString:@"document.body.style.background=transparent;"];
        //NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.zoom = 1.5;"];
        //[_anim stringByEvaluatingJavaScriptFromString:jsCommand];
        [_anim setOpaque:NO];
        [_anim setDelegate:self];
        [_anim setUserInteractionEnabled:NO];


        CGSize contentSize = _anim.scrollView.contentSize;
        CGSize viewSize = self.bounds.size;
        float rw = viewSize.width / contentSize.width;
        _anim.scrollView.minimumZoomScale = rw;
        _anim.scrollView.maximumZoomScale = rw;
        NSLog(@"c2a > kpsule anim scale : %.2f",rw);
        _anim.scrollView.zoomScale = rw;

        
        
        // WEBVIEW FOR KAPSULE
        _wv = [[UIWebView alloc] initWithFrame:CGRectMake(0,0,_w,_h)];
        _wv.delegate = self;
        _wv.multipleTouchEnabled = YES;
        _wv.scalesPageToFit = NO;
        [_wv setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        
        // Prevent page from bouncing
        for (id subview in _wv.subviews)
            if ([[subview class] isSubclassOfClass: [UIScrollView class]])
                ((UIScrollView *)subview).bounces = NO;
        
        NSURL* nsUrl = [NSURL URLWithString:_url];
        //NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
        NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
        [_wv loadRequest:request];
        
        [_wv setAllowsInlineMediaPlayback:YES];
        [_wv setDataDetectorTypes:UIDataDetectorTypeLink];
        [_wv setBackgroundColor: [UIColor colorWithRed:0. green:0.39 blue:0.106 alpha:0.]]; //[UIColor clearColor]]; //[UIColor blackColor]]; // // darkGrayColor [UIColor blackColor]]; //[UIColor clearColor]];
        [_wv setOpaque:NO];

        NSLog(@"c2a > loading page : [%@]",_url);
        _wv.hidden = YES;
        _anim.hidden = NO;
        
        [self addSubview:_wv];
        [self addSubview:_anim];
        
    }
    return self;
}

-(void)showWebView {
    _wv.hidden=NO;
}


-(void)drawHorizontal {
        NSLog(@"vote > drawHorizontal");
        int _imgh = _h-70;              //image heigth
        int _imgw = _w/2-20;            //_imgh*3/4;          //image width
        int _tw=_w-40;                  //title ctrl width
    
}

-(void)drawVertical {
        NSLog(@"vote > drawVertical");
        int _imgh = _h-70;              //image heigth
        int _imgw = _w/2-20;          //_imgh*3/4;          //image width
        int _tw=_h-20;                  //title ctrl width
    

}


-(void)webViewDidFinishLoad:(UIWebView *)webView {
    /*if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"]) {
        NSLog(@"c2a > page loaded : %@",_url);
    }*/
    NSString *status = [webView stringByEvaluatingJavaScriptFromString:@"document.readyState"];
    NSLog(@"c2a > page loaded : %@ (%@)",_url,status);
    //[webView stringByEvaluatingJavaScriptFromString:@"document.body.style.backgroundColor = 'rgba(255, 255, 255, 0.0)';"];
    
    CGSize contentSize = webView.scrollView.contentSize;
    CGSize viewSize = self.bounds.size;

    float rw = viewSize.width / contentSize.width;

    webView.scrollView.minimumZoomScale = rw;
    webView.scrollView.maximumZoomScale = rw;
    webView.scrollView.zoomScale = rw;
    

    
    if ([status isEqualToString:@"interactive"]) {
        //[blockView setEnabled:YES];
    }
    if ([status isEqualToString:@"complete"]) {
        //[blockView setEnabled:YES];
    }
    
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {

    /*NSLog(@"webview > full url     : %@", [request URL]);
    NSLog(@"webview > scheme       : %@", [[request URL] scheme]);
    NSLog(@"webview > host         : %@", [[request URL] host]);
    NSLog(@"webview > query        : %@", [[request URL] query]);
    NSLog(@"webview > fragment     : %@", [[request URL] fragment]);*/
    
    NSLog(@"c2a > preloading %@",[request URL]);
    
    return YES;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



@end
