//
//  BW.m
//  testAV3
//
//  Created by Cedric Hamelin on 28/01/14.
//  Copyright (c) 2014 Cedric Hamelin. All rights reserved.
//

#import "WidgetPage.h"

@implementation WidgetPage

- (id)initWithFrame:(CGRect)frame andWithDelegate:(id)delegate andWithUDID:(NSString *)udid {
    self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code
        self.frame=frame;
        NSLog(@"bw > (%f,%f) %fx%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);

        _w = frame.size.width;
        _h = frame.size.height;
        _x = frame.origin.x;
        _y = frame.origin.y;
        
        _index = _x/_w;
        NSLog(@"bw > current index in scrollview : %d",_index);
        
        _delegate = delegate;
        _wid = @"";
        _udid = udid;
        
        _mh = _h * 0.02;
        _mw = _w * 0.02;
        
        _widgetFrame = CGRectMake( _mw, _mh, _w-2*_mw, _h-2*_mh);
        
        title = [NSString stringWithFormat:@"widget page"];
        // BLURY BACKGROUND
        /*UIView *bg = [[UIView alloc] initWithFrame:_widgetFrame];
        bg.layer.borderColor = [UIColor blackColor].CGColor;
        bg.layer.borderWidth = 2.0;
        bg.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.4];
        */
        
        if ([self iOSVersionIsAtLeast:@"7.0"]) {
            bg = [[UIToolbar alloc] initWithFrame:CGRectMake( 10, 10, _w-20, _h-20)];
            //bg.barStyle = UIBarStyleDefault;
            bg.barStyle = UIBarStyleBlack; //UIBarStyleBlackTranslucent;//UIBarStyleBlack;
            bg.translucent=YES;
            bg.tintColor = [UIColor colorWithWhite:1.0 alpha:0.1];
            [self addSubview:bg];
        } else {
            sbg = [[UIView alloc] initWithFrame:CGRectMake( 10, 10, _w-20, _h-20)];
            sbg.layer.borderColor = [UIColor blackColor].CGColor;
            sbg.layer.borderWidth = 2.0;
            sbg.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.4];
            [self addSubview:sbg];
        }
        


        /*self.layer.borderColor = [UIColor redColor].CGColor;
        self.layer.borderWidth = 3.0f;*/

        self.userInteractionEnabled = YES;
    }
    return self;
}


-(void)drawHorizontal {
    NSLog(@"page > drawHorizontal");
    self.frame = CGRectMake(_index*_w,_y,_w,_h);
    _widgetFrame = CGRectMake( _mw, _mh, _w-2*_mw, _h-2*_mh);
    bg.frame = CGRectMake( 10, 10, _w-20, _h-20);
    sbg.frame = CGRectMake( 10, 10, _w-20, _h-20);
    if ([widget respondsToSelector:@selector(drawHorizontal)]) {
        [widget drawHorizontal];
    }
}

-(void)drawVertical {
    NSLog(@"page > drawVertical");
    self.frame = CGRectMake(_index*_h,_y,_h,_w);
    _widgetFrame = CGRectMake( _mh, _mw,_h-2*_mh, _w-2*_mw);
    bg.frame = CGRectMake( 10, 10, _h-20, _w-20);
    sbg.frame = CGRectMake( 10, 10, _h-20, _w-20);
    if ([widget respondsToSelector:@selector(drawVertical)]) {
        [widget drawVertical];
    }
}



- (NSDictionary*)processFeedBack:(NSDictionary*)fb {
    NSError *error;

    NSDictionary *feedback = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",_wid],@"widgetId",
                                                                        [NSString stringWithFormat:@"%@",_udid],@"deviceId",
                                                                        [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"timeStamp",
                                                                        fb,@"data",
                                                                        nil];
    NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithDictionary:@{@"result":@""}];

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:feedback
                                            options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                            error:&error];

    if (! jsonData) {
        NSLog(@"widget > feedback error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"widget > feedback [%@]: %@",_feedbackUrl,jsonString);
        
        //dispatch_queue_t myQueue = dispatch_queue_create("myQueue", NULL);
        // execute a task on that queue asynchronously
        //dispatch_async(myQueue, ^{
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                initWithURL:[NSURL 
                URLWithString:_feedbackUrl]];
         
        [request setHTTPMethod:@"POST"];
        [request setValue:@"text/json" forHTTPHeaderField:@"Content-type"];
        
        [request setValue:[NSString stringWithFormat:@"%d",[jsonString length]] forHTTPHeaderField:@"Content-length"];
         
        [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        [request setAllowsCellularAccess:YES];
        
        //[[NSURLConnection alloc] initWithRequest:request delegate:self];
        NSError * error = nil;
        NSURLResponse * response = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                          returningResponse:&response
                                                      error:&error];
        
        if(error==nil) {
            if(response!=nil) {
                //NSLog(@"widget > feedback response %@",[NSString stringWithUTF8String:[data bytes]]);
                //[result objectForKey:@"response"] = [data objectFromJSONData];
                NSError * error1 = nil;
                NSMutableDictionary *innerJson = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error1] copyItems:YES];
                //NSLog(@"widget > json %@",innerJson);
                //[result setValue:[innerJson copy] forKey:@"result"];
                return innerJson;
                
            }
        }
        //});
        
        
    }
    //NSLog(@"widget > feedback response %@",result);
    return result;

}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"widget > touch began!");
}


/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ onWidgetStart                                                                                    ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(BOOL)onWidgetStart:(NSDictionary *)wdata withTrigger:(NSDictionary *)trig {

    if (widget) {
        [[widget subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [widget removeFromSuperview];
        widget = nil;
    }
    
    if (![[[wdata objectForKey:@"widget"] allKeys] containsObject:@"native-code"]) {
        NSLog(@"widget > not for native code");
        return NO;
    }
    
    //NSLog(@"widget > %@",wdata);
    
    _wid = [[wdata objectForKey:@"widget"] objectForKey:@"id"];
    _feedbackUrl=[[[wdata objectForKey:@"widget"] objectForKey:@"header"] objectForKey:@"feedbackurl"];
    NSString *wType = @"";
    if ([[[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"TYPE"] isKindOfClass:[NSString class]]) {
        wType=[[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"TYPE"];
    }
    if ([[[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"TYPE"] isKindOfClass:[NSArray class]]) {
        wType=[[[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"TYPE"] objectAtIndex:0];
    }


    NSLog(@"widget > [%@] type %@",_wid,wType);
    if ([wType isEqualToString:@"RATE"]) {
        @try {
            NSString *t = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"TITLE"];
            NSString *d = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"DESCRIPTION"];
            NSString *b64img = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"POSTER"];
            UIImage *ret = [UIImage imageFromBase64String:b64img];
            widget = [[SimpleRate alloc] initWithFrame:_widgetFrame
                                         andWithDelegate:self
                                         andWithTitle:[NSString stringWithFormat:@"%@",t]
                                         andWithDescriptionText:[NSString stringWithFormat:@"%@",d]
                                         andWithImage:ret];
        }
        @catch(NSException *e) {
            NSLog(@"widget > malformed [rate] widget");
        }
        
    }
    if ([wType isEqualToString:@"VOTE"]) {
        @try {
            NSString *t = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"TITLE"];
            NSString *b64img1 = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"IVOTE1"];
            UIImage *im1 = [UIImage imageFromBase64String:b64img1];
            NSString *b64img2 = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"IVOTE2"];
            UIImage *im2 = [UIImage imageFromBase64String:b64img2];
            
            widget = [[SimpleVote alloc] initWithFrame:_widgetFrame
                                         andWithDelegate:self
                                         andWithTitle:[NSString stringWithFormat:@"%@",t]
                                         andWithImage1:im1
                                         andWithImage2:im2];
        }
        @catch(NSException *e) {
            NSLog(@"widget > malformed [vote] widget");
        }
        
    }
   if ([wType isEqualToString:@"QUIZZ"]) {
        @try {
            NSString *t = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"COMMENT"];
            NSString *c1 = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"CHOICE1"];
            NSString *c2 = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"CHOICE2"];
            NSString *c3 = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"CHOICE3"];
            NSString *b64img1 = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"PAGE"];
            UIImage *im1 = [UIImage imageFromBase64String:b64img1];
            
            widget = [[SimpleQuizz alloc] initWithFrame:_widgetFrame
                                         andWithDelegate:self
                                         andWithComment:[NSString stringWithFormat:@"%@",t]
                                         andWithImage:im1
                                         andWithChoices:@[c1,c2,c3]];
        }
        @catch(NSException *e) {
            NSLog(@"widget > malformed [quizz] widget");
        }
        
    }
   if ([wType isEqualToString:@"CALL2ACTION"]) {
        @try {
            NSString *t = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"TITLE"];
            NSString *url = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"URL"];
            NSString *b64zip = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"BANNER"];
            //UIImage *im1 = [UIImage imageFromBase64String:b64img];
            
            
            widget = [[SimpleC2A alloc] initWithFrame:_widgetFrame
                                         andWithDelegate:self
                                         andWithTitle:[NSString stringWithFormat:@"%@",t]
                                         andWithZip:b64zip
                                         andWithURL:url];
        }
        @catch(NSException *e) {
            NSLog(@"widget > malformed [call2action] widget");
        }
        
    }
   if ([wType isEqualToString:@"CHECKIN"]) {
        @try {
            NSString *channel = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"CHANNELID"];
            NSString *b64img = [[[wdata objectForKey:@"widget"] objectForKey:@"native-code"] objectForKey:@"BANNER"];
            UIImage *im1 = [UIImage imageFromBase64String:b64img];
            
            widget = [[SimpleCheckIn alloc] initWithFrame:_widgetFrame
                                         andWithDelegate:self
                                         andWithChannel:[NSString stringWithFormat:@"%@",channel]
                                         andWithImage:im1
                                         andWithTrigger:trig];
        }
        @catch(NSException *e) {
            NSLog(@"widget > malformed [checking] widget");
        }
        
    }

    
    if (widget) {
        [self addSubview:widget];
        return YES;
    }
    
    return NO;
}


/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ onWidgetStop                                                                                     ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
-(BOOL)onWidgetStop:(NSDictionary *)wdata {
    //NSLog(@"main > we received a widget STOP!");
    @try {
        /*@try {
            NSString *js = [[[wdata objectForKey:@"widget"] objectForKey:@"script-code"] objectForKey:@"stop"];
        }
        @catch(NSException *e) {
            //NSLog(@"main > problem with widget data in widget|script-code");
        }*/
        /*for (UIView *w in [widget subviews]) {
            [w removeFromSuperview];
        }*/
        [[widget subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
        [widget removeFromSuperview];
        widget = nil;
    }
    @catch(NSException *e) {
        NSLog(@"widget > problem with removing widget");
    }
    
    
    
    return YES;
}


-(BOOL)iOSVersionIsAtLeast:(NSString*)version {
    NSComparisonResult result = [[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch];
    return (result == NSOrderedDescending || result == NSOrderedSame);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
